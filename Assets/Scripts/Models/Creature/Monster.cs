using System;
using HeroLineWarsUnity;
using UnityEngine;
using HeroLineWarsUnity.Locations;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Etats;
using System.Collections.Generic;

namespace HeroLineWarsUnity.Creatures
{
 [RequireComponent(typeof(SphereCollider))]
    public class Monster : Creature
    {
        #region Variables

        public SphereCollider mHeroDetectionArea;

        public IAttackable mDefaultTarget;

        public bool contour;

        public string nom;

        public int vie;

        public int vieActuelle;

        public GameObject monsterPrefab;

        public int defense;

        public float vitesse;

        public int experience;

        public int or;

        private IEtatMonstre etat;

        public Animator animator;
        #endregion

        #region Built-in methods
        // Start is called before the first frame update
        protected new void Start()
        {
            animator = GetComponent<Animator>();
            mMovementSpeed = vitesse;
            base.Start();
            GetComponent<Outline>().enabled = false;
            // Initialisation de la zone de détection du Hero
            SetState(new EtatMonstreVivant(this));
            mHeroDetectionArea = GetComponent<SphereCollider>();
            mHeroDetectionArea.isTrigger = true;
            mHeroDetectionArea.radius = 6;

            // Initialisation de la cible par défaut
            mTarget = mDefaultTarget;
        }

        // Update is called once per frame
        protected new void Update()
        {
            base.Update();

            // Gestion de l'attaque
            float distanceFromTarget = Vector3.Distance(transform.position, mTarget.GetPosition());
            float currentTime = Time.time;

            if (distanceFromTarget <= mAttackRange && currentTime >= mNextAttackTimestamp)
            {
                Attack(mTarget);
            }
            else if (distanceFromTarget > mAttackRange)
            {
                attaqueEnCours = false;
            }

            if (vieActuelle <= 0)
            {
                SetState(new EtatMonstreMort(this));
            }

        }


        public void SetState(IEtatMonstre etat)
        {
            if (this.etat != null)
                this.etat.OnStateExit();

            this.etat = etat;
            

            if (etat != null)
                etat.OnStateEnter();
        }



        private void OnTriggerEnter(Collider other)
        {
            Hero detectedHero = other.GetComponent<Hero>();
            Nexus detectedNexus = other.GetComponent<Nexus>();
            if (detectedHero)
            {
                mTarget = detectedHero;
            }
            if (detectedNexus)
            {
                
                SetState(new EtatMonstreSacrifie(this));
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (mDefaultTarget != null)
            {
                mTarget = mDefaultTarget;
            }
        }
        
        #endregion

        #region Implemented methods

        
        
        public override void Attack(IAttackable target)
        {
            etat.Attack(target);
        }

        public override void TakeDamage(int amount)
        {
            etat.TakeDamage(amount);
        }

        protected override void UpdateNavAgentDestination()
        {
            etat.UpdateNavAgentDestination();
        }
        public override int GetCurrentHealthPoints()
        {
            return vieActuelle;
        }

        public override int GetMaxHealthPoints()
        {
            return vie;
        }

        #endregion
    }
}
