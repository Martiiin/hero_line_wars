using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HeroLineWarsUnity
{
    public class BarreVie : MonoBehaviour
    {
        #region Variables

        public Color mZeroHealthColor = Color.red;
        public Color mFullHealthColor = Color.green;
        public Slider mHealthSlider;
        public Image mHealthSliderImage;

        private IAttackable _mObservedAttackable;

        #endregion

        #region Built-in methods

        // Start is called before the first frame update
        void Start()
        {
            _mObservedAttackable = GetComponentInParent<IAttackable>();
        }

        // Update is called once per frame
        void Update()
        {
            UpdateHealthAmount();
            UpdateRotation();
        }

        #endregion

        #region Custom methods

        void UpdateHealthAmount()
        {
            float currentHP = _mObservedAttackable.GetCurrentHealthPoints();
            float maxHP = _mObservedAttackable.GetMaxHealthPoints();

            mHealthSliderImage.color = Color.Lerp(mZeroHealthColor, mFullHealthColor, currentHP / maxHP);
            mHealthSlider.value = currentHP / maxHP;
        }

        void UpdateRotation()
        {
            Camera gameCamera = Camera.main;
            transform.LookAt(gameCamera.transform.position, gameCamera.transform.rotation * Vector3.up);
        }

        #endregion

    }
}
