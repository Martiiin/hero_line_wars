using HeroLineWarsUnity;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWarsUnity.Creatures
{
    [RequireComponent(typeof(NavMeshAgent))]
    public abstract class Creature : MonoBehaviour, IAttackable
    {
        #region Variables

        [Header("Creature Properties")] 
        public IAttackable mTarget;

        [Header("Creature Stats")] 
        public int mHealthPoint = 100;
        public float mMovementSpeed = 1f;
        public float mDamage = 1f;
        public int mLevel = 1;
        public float mAttackRange = 2f;
        public float mNextAttackTimestamp = 1f;
        public float mAttackCooldown = 1f;
        public bool attaqueEnCours = false;

        #endregion

        public NavMeshAgent mNavMeshAgent;

        #region Built-in methods

        // Start is called before the first frame update
        protected void Start()
        {
            mNavMeshAgent = GetComponent<NavMeshAgent>();
            mNavMeshAgent.speed *= mMovementSpeed;
        }

        // Update is called once per frame
        protected void Update()
        {
            UpdateNavAgentDestination();
        }

        #endregion
        
        #region Implemented methods

        public int GetHealthPoints()
        {
            return mHealthPoint;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
        
        #endregion

        #region Abstract methods

        protected abstract void UpdateNavAgentDestination();
        public abstract void Attack(IAttackable target);
        public abstract void TakeDamage(int amount);

        public abstract int GetCurrentHealthPoints();


        public abstract int GetMaxHealthPoints();
 

        #endregion
    }


}
