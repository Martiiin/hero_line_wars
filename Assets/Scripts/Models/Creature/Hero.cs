using HeroLineWarsUnity;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Etats;
using HeroLineWarsUnity.Abilite;
using HeroLineWarsUnity.Controller;
using System.Collections.ObjectModel;
using HeroLineWarsUnity.Objet;
using UnityEngine.UI;
using TMPro;

namespace HeroLineWarsUnity.Creatures
{

    [RequireComponent(typeof(AbilityManager))]
    public class Hero : Creature
    {
        #region Variables



        [Header("Hero Properties")]
        public Camera mGameCamera;

        [Header("Hero Stats")]
        
        public int vieActuelle=5;
        public int ManaPoint = 100;
        public int manaActuel = 100;
        //public int Force = 10;
        //public int Dexterite = 10;
        //public int Intelligence = 10;
        //public int Endurance = 10;
        public float dureeMort =10f;
        public float tempsMort;
        public int defense;
        public int vie;
        public int experienceProchainNiveau = 100;
        public float vitesse = 10f;
        public int or=0;
        public int experience;
        protected IEtatHero etat;
        public Collection<Ability> mAbilities;
        private AbilityManager _mAbilityManager;
        public ObjetInventaire[] inventaire;

        private int vieAvantObjets;
        private int manaAvantObjets;
        private int defenseAvantObjets;
        private float dommagesAvantObjets;

        public Animator animator;

        public AudioSource audioSource;
        public AudioClip audioClip;

        public Slider mAbility1Slider;
        public Text mAbility1SliderText;
        public Slider mAbility2Slider;
        public Text mAbility2SliderText;
        public Slider mAbility3Slider;
        public Text mAbility3SliderText;
        public Slider mAbility4Slider;
        public Text mAbility4SliderText;

        public float mCooldownEndTimestamp1;
        public float mCooldownEndTimestamp2;
        public float mCooldownEndTimestamp3;
        public float mCooldownEndTimestamp4;


        #endregion

        #region Built-in methods
        // Start is called before the first frame update
        protected new void Start()
        {

            audioSource.volume = 0.2F;
            animator = GetComponent<Animator>();
            mMovementSpeed = vitesse;
            SetState(new EtatHeroVivant(this));
            base.Start();
            mAbilities = new Collection<Ability>();
            mAbilities.Add(new DamageZoneAbility(this, 2f, 0f,50));
            mAbilities.Add(new StatAbility(this, 20f, 10f,20)); //2
            mAbilities.Add(new HealAbility(this, 30f, 0f,30)); //3
            mAbilities.Add(new NukeAbility(this, 2f, 0f,100)); //4
            _mAbilityManager = GetComponent<AbilityManager>();
            inventaire = new ObjetInventaire[6];
            SauvegardeStatistiquesHeroAvantObjets();
        }

        // Update is called once per frame
        protected new void Update()
        {
            base.Update();

            UpdateAbilitySliders();



            if (etat.EstMort() && Time.time >= tempsMort)
            {
                SetState(new EtatHeroVivant(this));
            }

            if (experience >= experienceProchainNiveau)
            {
                NiveauSuperieur();
            }

            HandleAbilities();

            // Gestion de l'attaque
            if(mTarget != null) 
            { 
                float distanceFromTarget = Vector3.Distance(transform.position, mTarget.GetPosition());
                float currentTime = Time.time;
                if (distanceFromTarget <= mAttackRange && currentTime >= mNextAttackTimestamp)
                {
                    Attack(mTarget); 
                }
                else if (distanceFromTarget > mAttackRange)
                {
                    attaqueEnCours = false;
                }
            }
            else
            {
                attaqueEnCours = false;
            }

        }
        #endregion

        #region Implemented methods


        private void HandleAbilities()
        {
            
            // Lancement d'une nouvelle capacité
            if (!_mAbilityManager.IsAbilityStillExecuting())
            {
                Ability launchedAbility = null;
                if (Input.GetAxis("Ability1") > 0)
                {
                    //launchedAbility = (Ability)mAbilities[0].Clone();
                    launchedAbility = mAbilities[0];
                }
                else if (Input.GetAxis("Ability2") > 0)
                {
                    launchedAbility = mAbilities[1];
                }
                else if (Input.GetAxis("Ability3") > 0)
                {
                    launchedAbility = mAbilities[2];
                }
                else if (Input.GetAxis("Ability4") > 0)
                {
                    launchedAbility = mAbilities[3];
                }

                if (launchedAbility != null)
                {
                    
                    _mAbilityManager.SetStrategy(launchedAbility);
                    _mAbilityManager.Update();
                    
                }
            }
           
        }


            public void SetState(IEtatHero etat)
        {
            if (this.etat != null)
                this.etat.OnStateExit();

            this.etat = etat;


            if (etat != null)
                etat.OnStateEnter();
        }


        public override void Attack(IAttackable target)
        {
            etat.Attack(target);
        }

        public override void TakeDamage(int amount)
        {
            etat.TakeDamage(amount);

        }

        protected override void UpdateNavAgentDestination()
        {
            etat.UpdateNavAgentDestination();
        }




        protected  void OLDUpdateNavAgentDestination()
        {
            if (!mGameCamera)
            {
                Debug.Log("Failed to UpdateNavAgentDestination : Missing Camera Reference.");
                return;
            }

            // TODO : à modifier lorqu'on aura un UI pour ne pas prendre en compte les clics dans l'UI
            if (Input.GetMouseButton(0))
            {
                Ray ray = mGameCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    mNavMeshAgent.SetDestination(hitInfo.point);
                }
            }
        }

        public void NiveauSuperieur()
        {
            mLevel += 1;
            experienceProchainNiveau = experienceProchainNiveau + 100;
            experience = 0;
            MiseAJourStatsNiveau();
            MiseAjourStatsEquipement();
        }

        public void MiseAJourStatsNiveau()
        {
            vie = vieAvantObjets +100;
            vieActuelle = vie;
            ManaPoint = manaAvantObjets +100;
            defense = defenseAvantObjets +1;
            mDamage = dommagesAvantObjets +5;
            SauvegardeStatistiquesHeroAvantObjets();

        }

        public void SauvegardeStatistiquesHeroAvantObjets() {

            vieAvantObjets = vie;
            manaAvantObjets = ManaPoint;
            defenseAvantObjets = defense;
            dommagesAvantObjets = mDamage;
        }

        public void MiseAjourStatsEquipement()
        {
            int vieAjout=0;
            int manaAjout=0;
            int defenseAjout=0;
            int mDamageAjout=0;

            for (int i = 0; i < inventaire.Length; i++)
            {
                if(inventaire[i] != null && !(inventaire[i] is IConsommable))
                {
                    vieAjout = vieAjout + inventaire[i].getBonusVie();
                    manaAjout = manaAjout + inventaire[i].getBonusMana();
                    defenseAjout = defenseAjout + inventaire[i].getBonusDefense();
                    mDamageAjout = mDamageAjout + inventaire[i].getBonusDommage();
                }
            }
            vie = vieAvantObjets + vieAjout;
            ManaPoint = manaAvantObjets + manaAjout;
            defense = defenseAvantObjets + defenseAjout;
            mDamage = dommagesAvantObjets + mDamageAjout;
        }

        public override int GetCurrentHealthPoints()
        {
            return vieActuelle;
        }

        public override int GetMaxHealthPoints()
        {
            return vie;
        }
        #endregion

        void UpdateAbilitySliders()
        {
            float ability1CooldownLeft = mAbilities[0].mCooldownEndTimestamp - Time.time;
            float ability1CooldownRatio = ability1CooldownLeft / mAbilities[0].mCooldown;

            mAbility1Slider.value = 1 - ability1CooldownRatio;
            if (ability1CooldownLeft > 0f)
                mAbility1SliderText.text = ability1CooldownLeft.ToString("0.00");
            else
                mAbility1SliderText.text = "OK";

          float  ability2CooldownLeft = mAbilities[1].mCooldownEndTimestamp - Time.time;
          float  ability2CooldownRatio = ability2CooldownLeft / mAbilities[1].mCooldown;

            mAbility2Slider.value = 1 - ability2CooldownRatio;
            if (ability2CooldownLeft > 0f)
                mAbility2SliderText.text = ability2CooldownLeft.ToString("0.00");
            else
                mAbility2SliderText.text = "OK";

            float ability3CooldownLeft = mAbilities[2].mCooldownEndTimestamp - Time.time;
            float ability3CooldownRatio = ability3CooldownLeft / mAbilities[2].mCooldown;

            mAbility3Slider.value = 1 - ability3CooldownRatio;
            if (ability3CooldownLeft > 0f)
                mAbility3SliderText.text = ability3CooldownLeft.ToString("0.00");
            else
                mAbility3SliderText.text = "OK";

            float ability4CooldownLeft = mAbilities[3].mCooldownEndTimestamp - Time.time;
            float ability4CooldownRatio = ability4CooldownLeft / mAbilities[3].mCooldown;

            mAbility4Slider.value = 1 - ability4CooldownRatio;
            if (ability4CooldownLeft > 0f)
                mAbility4SliderText.text = ability4CooldownLeft.ToString("0.00");
            else
                mAbility4SliderText.text = "OK";

        }
    }
}
