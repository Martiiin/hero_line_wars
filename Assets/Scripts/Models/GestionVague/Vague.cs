using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity.GestionVague
{
    public class Vague 
    {


        public int numVague;
        public List<GroupeMonstres> MonstresDansVague;

        public Vague(int numVagueToAdd, List<GroupeMonstres> MonstresDansVagueToAdd)
        {
            this.numVague = numVagueToAdd;
            this.MonstresDansVague = MonstresDansVagueToAdd;
        }

    }
}
