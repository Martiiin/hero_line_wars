using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Dictionnaires;

namespace HeroLineWarsUnity.GestionVague
{
    public class GroupeMonstres
    {

        public int nombreMonstres;
        public MonsterParameters typeMonstre;

        public GroupeMonstres(int nombreMonstresToAdd, MonsterParameters typeMonstreToAdd)
        {
            this.nombreMonstres = nombreMonstresToAdd;
            this.typeMonstre = typeMonstreToAdd;
        }
    }
}
