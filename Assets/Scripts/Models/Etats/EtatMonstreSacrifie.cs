using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Controller;

namespace HeroLineWarsUnity.Etats
{
    public class EtatMonstreSacrifie : IEtatMonstre
    {
        public EtatMonstreSacrifie(Monster monstre) : base(monstre)
        {

        }


        public override void Attack(IAttackable target)
        {
            return;
        }

        public override void TakeDamage(int amount)
        {

            return;
        }

        public override void UpdateNavAgentDestination()
        {
            monstre.mNavMeshAgent.destination = monstre.transform.position;

        }


        public override void OnStateEnter()
        {
            monstre.animator.SetTrigger("Sacrifice");
            Health health = GameObject.Find("HealthManager").GetComponent<Health>();
            health.health -= 1;
            //animation de mort//
            
            GameObject.Find("GameManager").GetComponent<GameManager>().removeHeroTarget(monstre);
            GameObject test = this.monstre.gameObject;
            GameObject.Find("GameManager").GetComponent<GameManager>()._mMonsterInstances.Remove(test);
            GameObject.Destroy(test);
   


        }
        public override void OnStateExit()
        {


        }
    }
}


