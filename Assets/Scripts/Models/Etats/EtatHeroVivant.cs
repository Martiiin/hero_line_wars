using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Creatures;
using UnityEngine.EventSystems;

namespace HeroLineWarsUnity.Etats
{
    public class EtatHeroVivant : IEtatHero
    {
        public EtatHeroVivant(Hero hero) : base(hero)
        {

        }


        public override void Attack(IAttackable target)
        {
            hero.animator.SetTrigger("Attack");
            hero.attaqueEnCours = true;    
            target.TakeDamage((int)hero.mDamage);
            hero.mNextAttackTimestamp = Time.time + hero.mAttackCooldown;
        }

        public override void TakeDamage(int amount)
        {
            int playSound = Random.Range(1, 5);
            if (playSound == 1)
            {
                hero.audioSource.PlayOneShot(hero.audioClip);
            }
            hero.vieActuelle -= amount;
            if (hero.vieActuelle <= 0)
            {
                hero.SetState(new EtatHeroMort(hero));
            }
        }

        public override void UpdateNavAgentDestination()
        {
            if (hero.attaqueEnCours)
            {
                hero.mNavMeshAgent.destination = hero.transform.position;
            }

            else if (hero.mTarget != null)
            {
                hero.mNavMeshAgent.SetDestination(hero.mTarget.GetPosition());
            }

            // `!EventSystem.current.IsPointerOverGameObject()` emp�che de prendre en compte les clics sur l'UI
            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Ray ray = hero.mGameCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    Monster clickedMonster = hitInfo.transform.gameObject.GetComponent<Monster>();

                    if (clickedMonster)
                    {
                        hero.mTarget = clickedMonster;

                    }
                    else
                    {
                        hero.mNavMeshAgent.SetDestination(hitInfo.point);
                        hero.mTarget = null;
                    }
                }
            }
        }

        public override void OnStateEnter()
        {
            hero.vieActuelle = hero.vie;
            hero.animator.SetBool("Alive", true);

        }
        public override void OnStateExit()
        {


        }

        public override bool EstMort()
        {
            return false;
        }
    }
}
