using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Controller;

namespace HeroLineWarsUnity.Etats
{
    public class EtatMonstreMort : IEtatMonstre
    {
        public EtatMonstreMort(Monster monstre):base (monstre)
        {

        }


        public override void Attack(IAttackable target)
        {
            return;
        }

        public override void TakeDamage(int amount) 
        {

            return;
        }

        public override void UpdateNavAgentDestination()
        {
            monstre.mNavMeshAgent.destination=monstre.transform.position;

        }


        public override void OnStateEnter() {
            monstre.animator.SetBool("Alive", false);
            Debug.Log("check mort");
            GameObject.Find("GameManager").GetComponent<GameManager>().removeHeroTarget(monstre);
            GameObject.Find("Hero").GetComponent<Hero>().experience += monstre.experience;
            GameObject.Find("Hero").GetComponent<Hero>().or += monstre.or;
            //animation de mort//
            
            GameObject test = this.monstre.gameObject;
            GameObject.Find("GameManager").GetComponent<GameManager>()._mMonsterInstances.Remove(test);
            GameObject.Destroy(test);



        }
        public override void OnStateExit() {

 
        }


    }
}
