using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Creatures;
using UnityEngine.EventSystems;

namespace HeroLineWarsUnity.Etats
{
    public class EtatHeroMort : IEtatHero
    {
        public EtatHeroMort(Hero hero) : base(hero)
        {

        }


        public override void Attack(IAttackable target)
        {

        }

        public override void TakeDamage(int amount)
        {
            

        }

        public override void UpdateNavAgentDestination()
        {
            hero.mNavMeshAgent.destination = hero.transform.position;
        }

        public override void OnStateEnter()
        {
            hero.animator.SetBool("Alive", false) ;
            Vector3 respawnPosition=GameObject.Find("HeroRespawn").transform.position;
            Debug.Log("Position Nexus" + respawnPosition);
            //Vector3 decalage = new Vector3(0, 0, -10);
            hero.mNavMeshAgent.SetDestination(respawnPosition);
            hero.transform.position = respawnPosition;
            hero.tempsMort = Time.time + hero.dureeMort;
            Debug.Log("Temps de mort : " + hero.tempsMort);


        }
        public override void OnStateExit()
        {


        }

        public override bool EstMort()
        {
            return true;
        }
    }
}
