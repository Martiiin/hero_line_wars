using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Controller;

namespace HeroLineWarsUnity.Etats
{
    public class EtatMonstreVivant : IEtatMonstre
    {
        public EtatMonstreVivant(Monster monstre):base (monstre)
        {

        }

        public override void Attack(IAttackable target)
        {
            monstre.animator.SetTrigger("Attack");
            monstre.attaqueEnCours = true;
            target.TakeDamage((int)monstre.mDamage);
            monstre.mNextAttackTimestamp = Time.time + monstre.mAttackCooldown;
        }

        public override void TakeDamage(int amount) 
        {
            if (monstre.defense >= amount)
            {
                monstre.vieActuelle -= 1;
            }
            else
            {
                monstre.vieActuelle -= amount;
            }

        }

        public override void UpdateNavAgentDestination()
        {
            if (monstre.mTarget != null && !monstre.attaqueEnCours)
            {
                monstre.mNavMeshAgent.destination = monstre.mTarget.GetPosition();
            }
            else if(monstre.mTarget != null && monstre.attaqueEnCours){
                monstre.mNavMeshAgent.destination = monstre.transform.position;
            }
        }

        public override void OnStateEnter() {

            monstre.animator.SetBool("Alive", true);

        }
        public override void OnStateExit() {

            
        }
    }
}
