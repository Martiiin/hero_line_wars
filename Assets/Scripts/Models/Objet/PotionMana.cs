using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Creatures;


namespace HeroLineWarsUnity
{
    public class PotionMana : Consommable
    {
        int manaRendu;
        public override void effetConsommable()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().manaActuel > GameObject.Find("Hero").GetComponent<Hero>().ManaPoint)
            {
                GameObject.Find("Hero").GetComponent<Hero>().manaActuel = GameObject.Find("Hero").GetComponent<Hero>().ManaPoint;
            }
            else
            {
                GameObject.Find("Hero").GetComponent<Hero>().manaActuel += manaRendu;
            }
        }



       public PotionMana(string nom, int prix, int revente, int manaRendu)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.manaRendu = manaRendu;
        }

        public override ObjetInventaire Clone()
        {
            return new PotionMana(nom, prix, revente, manaRendu);
        }

    }
}
