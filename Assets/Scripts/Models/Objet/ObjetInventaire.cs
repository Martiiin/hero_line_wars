using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity.Objet
{
    public abstract class ObjetInventaire 
    {

        public string nom;
        public int prix;
        public int revente;

  

        public virtual int getBonusVie()
        {
            return 0;
        }

        public virtual int getBonusMana()
        {
            return 0;
        }

        public virtual int getBonusDefense()
        {
            return 0;
        }

        public virtual int getBonusDommage()
        {
            return 0;
        }

        public virtual ObjetInventaire Clone()
        {
            return null;
        }

    }
}
