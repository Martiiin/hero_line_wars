using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity.Objet
{
    public class Equipement : ObjetInventaire
    {
        int bonusVie;
        int bonusMana;
        int bonusDefense;
        int bonusDommage;

        public Equipement(string nom, int prix, int revente, int bonusVie, int bonusMana, int bonusDefense, int bonusDommage)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.bonusVie = bonusVie;
            this.bonusMana = bonusMana;
            this.bonusDefense = bonusDefense;
            this.bonusDommage = bonusDommage;

        }

        public override int getBonusVie()
        {
            return bonusVie;
        }

        public override int getBonusMana()
        {
            return bonusMana;
        }

        public override int getBonusDefense()
        {
            return bonusDefense;
        }

        public override int getBonusDommage()
        {
            return bonusDommage;
        }

        public override ObjetInventaire Clone()
        {
            return new Equipement(nom, prix, revente, bonusVie, bonusMana, bonusDefense, bonusDommage);
        }
    }
}
