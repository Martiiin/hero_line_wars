using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Interfaces;

namespace HeroLineWarsUnity.Objet
{
    public abstract class Consommable:ObjetInventaire, IConsommable
    {
        public int nombreCharge = 1;

        public void consommerCharge()
        {
            nombreCharge -= 1;
            effetConsommable();
        }

        public abstract void effetConsommable();


        public override ObjetInventaire Clone()
        {
            return null;
        }
    }

  
}
