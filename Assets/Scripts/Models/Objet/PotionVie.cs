using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity
{
    public class PotionVie : Consommable
    {
        int vieRendue;
        public override void effetConsommable()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().vieActuelle+vieRendue > GameObject.Find("Hero").GetComponent<Hero>().vie)
            {
                GameObject.Find("Hero").GetComponent<Hero>().vieActuelle = GameObject.Find("Hero").GetComponent<Hero>().vie;
            }
            else
            {
                GameObject.Find("Hero").GetComponent<Hero>().vieActuelle += vieRendue;
            }
            
        }



       public PotionVie(string nom, int prix, int revente, int vieRendue)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.vieRendue = vieRendue;
        }

        public override ObjetInventaire Clone()
        {
            return new PotionVie(nom, prix, revente, vieRendue);
        }
    }
}
