using HeroLineWarsUnity.Creatures;
using UnityEngine;
using UnityEngine.AI;

namespace HeroLineWarsUnity.Abilite
{
    public class StatAbility : Ability
    {
        public StatAbility(Hero hero, float cooldown, float duration, int cout) : base(hero, cooldown, duration, cout) { }

        public bool besoinRafraichirStats = false;
        public bool effetApplique = false;

        public override void Execute()
        {
            abilityEnCooldown();
            Debug.Log("COOLDOWN ? :" + enCooldown);

            //Debug.Log("Time.time : " + Time.time);
            if ((GameObject.Find("Hero").GetComponent<Hero>().manaActuel < cout || enCooldown) && !effetApplique)
            {
                Debug.Log("NO");
            }
            else
            {
                if ((Time.time < mEndTimestamp -2F) && effetApplique)
                {

                    Debug.Log("EN COURS : FIN LE : " + mEndTimestamp);
                    Debug.Log("Nous sommes : " + Time.time);
                }

                
                else if (Time.time >= (mEndTimestamp -2F) && besoinRafraichirStats && effetApplique)
                {
                    Debug.Log("Refresh Stats");
                    besoinRafraichirStats = false;
                    GameObject.Find("Hero").GetComponent<NavMeshAgent>().speed -= 10;
                }
                else if (enCooldown && !besoinRafraichirStats)
                {
                    Debug.Log("Effet Applique False");
                    effetApplique = false;
                }
                else if (GameObject.Find("Hero").GetComponent<Hero>().manaActuel >= cout || !enCooldown)
                {
                    Debug.LogWarning("TICK");
                    GameObject.Find("Hero").GetComponent<Hero>().manaActuel -= cout;
                    GameObject.Find("Hero").GetComponent<NavMeshAgent>().speed += 10;
                    mCooldownEndTimestamp = Time.time + mCooldown;
                    besoinRafraichirStats = true;
                    effetApplique = true;
                }
                else
                {
                    Debug.LogWarning("YAR");
                }
            }
  
        }
        public override object Clone()
        {
            StatAbility clone = new StatAbility(_mHero, mCooldown, mDuration, cout);
            return new StatAbility(_mHero, mCooldown, mDuration, cout);
        }
    }
}