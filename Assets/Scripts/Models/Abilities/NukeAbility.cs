using HeroLineWarsUnity.Creatures;
using UnityEngine;

namespace HeroLineWarsUnity.Abilite
{
    public class NukeAbility : Ability
    {
        public NukeAbility(Hero hero, float cooldown, float duration, int cout) : base(hero, cooldown, duration, cout) { }

        public override void Execute()
        {
            abilityEnCooldown();
            Debug.LogWarning("Abilite en cooldown encore ? "+ enCooldown);
            Debug.Log("Execute");

            //Debug.Log("Time.time : " + Time.time);
            if (GameObject.Find("Hero").GetComponent<Hero>().manaActuel < cout || enCooldown)
            {

            }
            else 
            {
                Debug.LogWarning("TICK");
                GameObject.Find("Hero").GetComponent<Hero>().manaActuel -= cout;
                Collider[] colliders = Physics.OverlapSphere(_mHero.transform.position, 3f);
                foreach (var collider in colliders)
                {
                    Monster currentMonster = collider.GetComponent<Monster>();
                    if (currentMonster)
                    {
                        int appliedDamage = Mathf.RoundToInt(_mHero.mDamage) * 33;
                        currentMonster.TakeDamage(appliedDamage);
                    }
                }
                mCooldownEndTimestamp = Time.time + mCooldown;
            }
        }
        public override object Clone()
        {
            NukeAbility clone = new NukeAbility(_mHero, mCooldown, mDuration, cout);
            return new NukeAbility(_mHero, mCooldown, mDuration, cout);
        }
    }
}