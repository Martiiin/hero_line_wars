using HeroLineWarsUnity.Creatures;
using UnityEngine;

namespace HeroLineWarsUnity.Abilite
{
    public class DamageZoneAbility : Ability
    {
        public DamageZoneAbility(Hero hero, float cooldown, float duration, int cout) : base(hero, cooldown, duration, cout) { }

        public override void Execute()
        {
            abilityEnCooldown();
            Debug.LogWarning("Abilite en cooldown encore ? "+ enCooldown);
            Debug.Log("Execute");

            //Debug.Log("Time.time : " + Time.time);
            if (GameObject.Find("Hero").GetComponent<Hero>().manaActuel < cout || enCooldown)
            {

            }
            else 
            {
                Debug.LogWarning("TICK");
                GameObject.Find("Hero").GetComponent<Hero>().manaActuel -= cout;
                Collider[] colliders = Physics.OverlapSphere(_mHero.transform.position, 2f);
                foreach (var collider in colliders)
                {
                    Monster currentMonster = collider.GetComponent<Monster>();
                    if (currentMonster)
                    {
                        int appliedDamage = Mathf.RoundToInt(_mHero.mDamage*0.3f);
                        currentMonster.TakeDamage(appliedDamage);
                    }
                }
                mCooldownEndTimestamp = Time.time + mCooldown;
            }
        }
        public override object Clone()
        {
            DamageZoneAbility clone = new DamageZoneAbility(_mHero, mCooldown, mDuration, cout);
            return new DamageZoneAbility(_mHero, mCooldown, mDuration, cout);
        }
    }
}