using HeroLineWarsUnity.Creatures;
using UnityEngine;

namespace HeroLineWarsUnity.Abilite
{
    public class HealAbility : Ability
    {
        public HealAbility(Hero hero, float cooldown, float duration, int cout) : base(hero, cooldown, duration, cout) { }

        public override void Execute()
        {
            abilityEnCooldown();
            Debug.LogWarning("Abilite en cooldown encore ? "+ enCooldown);
            Debug.Log("Execute");

            //Debug.Log("Time.time : " + Time.time);
            if (GameObject.Find("Hero").GetComponent<Hero>().manaActuel < cout || enCooldown)
            {

            }
            else 
            {
                Debug.LogWarning("TICK");
                GameObject.Find("Hero").GetComponent<Hero>().manaActuel -= cout;
                GameObject.Find("Hero").GetComponent<Hero>().vieActuelle += 100;
                mCooldownEndTimestamp = Time.time + mCooldown;
            }
        }
        public override object Clone()
        {
            HealAbility clone = new HealAbility(_mHero, mCooldown, mDuration, cout);
            return new HealAbility(_mHero, mCooldown, mDuration, cout);
        }
    }
}