using System;
using HeroLineWarsUnity.Creatures;
using UnityEngine;
using HeroLineWarsUnity.Controller;

namespace HeroLineWarsUnity.Abilite
{
    public abstract class Ability : ICloneable
    {
        #region Variables

        public float mCooldown;
        public float mDuration;
        public float mEndTimestamp;
        protected Hero _mHero;
        public int cout;
        public bool enCooldown;
        public float mCooldownEndTimestamp;

        #endregion

        public Ability(Hero hero, float cooldown, float duration, int cout)
        {
            mCooldown = cooldown;
            mDuration = duration;
            _mHero = hero;
            this.cout = cout;
            enCooldown = false;
            mCooldownEndTimestamp = 0;





        }

        public void SetEndTimestamp()
        {
            mEndTimestamp = Time.time + mDuration;
            
            Debug.LogWarning("TIME STAMP : "+mEndTimestamp);
            
        }

        public void abilityEnCooldown()
        {
            Debug.LogWarning("Cooldown ? : " + mCooldown);
            enCooldown = (Time.time - mCooldownEndTimestamp <= 0);
        }
        public abstract void Execute();

        public abstract object Clone();
    }
}