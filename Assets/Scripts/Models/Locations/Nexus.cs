using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity.Locations
{
    public class Nexus : MonoBehaviour, IAttackable
    {
        public void Attack(IAttackable target)
        {
            throw new System.NotImplementedException();
        }

        public int GetHealthPoints()
        {
            throw new System.NotImplementedException();
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void TakeDamage(int damageAmout)
        {
            Debug.Log("Une vie en moins :/");
        }

        public  int GetCurrentHealthPoints()
        {
            throw new System.NotImplementedException();
        }

        public  int GetMaxHealthPoints()
        {
            throw new System.NotImplementedException();
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
