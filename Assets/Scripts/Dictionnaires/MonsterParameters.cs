using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity.Dictionnaires
{
    public class MonsterParameters
    {
        public SphereCollider mHeroDetectionArea;

        public IAttackable mDefaultTarget;

        public string nom;

        public int vie;

        public GameObject monsterPrefab;

        public int defense;

        public float vitesse;

        public int or;

        public int experience;

        public MonsterParameters(string nom, int vie, GameObject monsterPrefab, int defense, float vitesse, int or, int experience)
        {
            this.nom = nom;
            this.vie = vie;
            this.monsterPrefab = monsterPrefab;
            this.defense = defense;
            this.vitesse = vitesse;
            this.or = or;
            this.experience = experience;
        }
    }
}
