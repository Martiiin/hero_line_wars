using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Factories;
using HeroLineWarsUnity.GestionVague;

namespace HeroLineWarsUnity.Dictionnaires
{
    public class DictionnaireMonstres
    {
       public  List<MonsterParameters> ListeDesMonstres = new List<MonsterParameters>();

        public DictionnaireMonstres()
        {
            this.ListeDesMonstres.Add(new MonsterParameters("Goblin", 50, Resources.Load<GameObject>("GruntHP"), 0, 1f,50,100));
            this.ListeDesMonstres.Add(new MonsterParameters("GrosGoblin", 100, Resources.Load<GameObject>("GruntHP"), 0, 2f,100,200));
            this.ListeDesMonstres.Add(new MonsterParameters("MegaGoblin", 200, Resources.Load<GameObject>("GruntHP"), 5, 2f,200,500));
            this.ListeDesMonstres.Add(new MonsterParameters("Lich", 100, Resources.Load<GameObject>("FreeLichHP"), 10, 3f, 100, 200));
            this.ListeDesMonstres.Add(new MonsterParameters("GrosLich", 200, Resources.Load<GameObject>("FreeLichHP"), 15, 3f, 200, 500));
            this.ListeDesMonstres.Add(new MonsterParameters("MegaLich", 500, Resources.Load<GameObject>("FreeLichHP"), 15, 4f, 200, 500));

        }

        public  MonsterParameters getMonsterModelByName(string Name)
        {
            return ListeDesMonstres.Find(x => x.nom.Contains(Name));
        }
  
           
           

     



    }
}
