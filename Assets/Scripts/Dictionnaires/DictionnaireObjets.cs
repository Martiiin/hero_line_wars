using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Factories;

namespace HeroLineWarsUnity.Dictionnaires
{
    public class DictionnaireObjets 
    {
        public List<ObjetInventaire> listeEquipement = new List<ObjetInventaire>();

        public DictionnaireObjets()
        {
            listeEquipement.Add(new PotionVieFactory("grande potion vie", 20, 10, 100).CreeObjetInvenaitre());
            listeEquipement.Add(new PotionManaFactory("grande potion mana", 20, 10, 100).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("armure", 100, 50, 50,0,5,0).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("hache", 100, 50, 0, 0, 0, 5).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("casque", 50, 25, 20, 0, 3, 0).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("bottes", 30, 15, 10, 0, 2, 0).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("gants", 20, 10, 5, 0, 2, 0).CreeObjetInvenaitre());
            listeEquipement.Add(new EquipementFactory("bouclier", 100, 50, 20, 0, 10, 0).CreeObjetInvenaitre());
            listeEquipement.Add(new PotionVieFactory("petite potion vie", 100, 50, 50).CreeObjetInvenaitre());
            listeEquipement.Add(new PotionManaFactory("petite potion mana", 100, 50, 50).CreeObjetInvenaitre());
        }


        public ObjetInventaire GetObjetByNom(string Name)
        {
            return listeEquipement.Find(x => x.nom.Contains(Name));
        }
    }
}
