using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.GestionVague;
using HeroLineWarsUnity.Factories;
using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity.Dictionnaires
{
    public class DictionnaireVagues
    {
        public  List<Vague> ListeVagues = new List<Vague>();
        public MonsterFactory monsterfactory;

        public DictionnaireVagues(DictionnaireMonstres dico)
        {
            monsterfactory = new MonsterFactory();
            UpdateList(dico);
        }


        public  void UpdateList(DictionnaireMonstres dico)
        {
            
            GenerationVague(1, "Goblin", 1, dico);
            GenerationVague(2, "Goblin", 3, dico);
            GenerationVague(3, "GrosGoblin", 1, dico);
            GenerationVague(4, "GrosGoblin", 2, dico);
            GenerationVague(5, "MegaGoblin", 1, dico);
            GenerationVague(6, "Lich", 1, dico);
            GenerationVague(7, "Lich", 3, dico);
            GenerationVague(8, "GrosLich", 1, dico);
            GenerationVague(9, "GrosLich", 2, dico);
            GenerationVague(10, "MegaLich", 1, dico);
            GenerationVague(11, "MegaLich", 2, dico);
            GenerationVague(12, "MegaLich", 3, dico);
            GenerationVague(13, "MegaLich", 5, dico);
            GenerationVague(14, "MegaLich", 10, dico);
            GenerationVague(15, "MegaLich", 15, dico);
            GenerationVague(16, "MegaLich", 20, dico);

        }

        public  Vague getVagueByNum(int vagueNumero)
        {
            return ListeVagues.Find(x => x.numVague ==vagueNumero);
        }

        public void GenerationVague(int numVague, string nomMonstre, int nombreMonstres, DictionnaireMonstres dico)
        {
            List<GroupeMonstres> ListeMonstresVague = new List<GroupeMonstres>();
            MonsterParameters monstre = monsterfactory.getMonster(dico, nomMonstre);
            GroupeMonstres groupeMonstres = new GroupeMonstres(nombreMonstres, monstre);
            ListeMonstresVague.Add(groupeMonstres);
            ListeVagues.Add(new Vague(numVague, ListeMonstresVague));
        }
    }
}
