using HeroLineWarsUnity.Creatures;
namespace HeroLineWarsUnity.Interfaces
{
    public abstract class IEtatHero
    {

        protected Hero hero;

        public virtual void OnStateEnter() { }
        public virtual void OnStateExit() { }

        public virtual void Attack(IAttackable target) { }

        public virtual void TakeDamage(int amount) { }

        public virtual void UpdateNavAgentDestination() { }

        public IEtatHero(Hero hero)
        {
            this.hero = hero;
        }

        public virtual bool EstMort() {
            return false;
        }
    }
}