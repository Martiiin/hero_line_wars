using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity.Interfaces
{
    public abstract class IEtatMonstre
    {
      
    protected Monster monstre;

        public virtual void OnStateEnter() { }
        public virtual void OnStateExit() { }

        public virtual void Attack(IAttackable target) { }

        public virtual void TakeDamage(int amount) { }

        public virtual void UpdateNavAgentDestination() { }

        public IEtatMonstre(Monster monstre)
        {
            this.monstre = monstre;
        }
    }
}