using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public interface IAttackable
    {
        public void Attack(IAttackable target);

        public void TakeDamage(int damageAmout);
        public int GetCurrentHealthPoints();

        public int GetMaxHealthPoints();
        public Vector3 GetPosition();
    }
}

