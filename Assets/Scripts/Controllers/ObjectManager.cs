using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Dictionnaires;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity
{
    public class ObjectManager 
    {
        public DictionnaireObjets dictionnaireObjets;
        // Start is called before the first frame update
        public ObjectManager()
        {
            dictionnaireObjets = new DictionnaireObjets();
        }

        public void AjouterObjetDansInventaire(ObjetInventaire objetInventaire)
        {
            if (InventairePlein()){
                return;
            }

            for (int i = 0; i < GameObject.Find("Hero").GetComponent<Hero>().inventaire.Length; i++)
            {
                if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[i] == null)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[i] = objetInventaire;
                    GameObject.Find("Hero").GetComponent<Hero>().MiseAjourStatsEquipement();
                    return;
                }
            }
            Debug.Log("Inventaire Plein");
        }

        public bool InventairePlein()
        {

            for (int i = 0; i < GameObject.Find("Hero").GetComponent<Hero>().inventaire.Length; i++)
            {
                if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[i] == null)
                {
                    return false;
                }
            }
            return true;
        }

        public void RetirerObjetDeLinventaire(int numObjet)
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[numObjet] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().inventaire[numObjet]= null;
                GameObject.Find("Hero").GetComponent<Hero>().MiseAjourStatsEquipement();
                
            }
        }

        public ObjetInventaire Clone(ObjetInventaire objetInventaire)
        {
            return objetInventaire.Clone();
        }

    }
}
