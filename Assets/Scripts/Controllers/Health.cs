using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using HeroLineWarsUnity.Controller;

public class Health : MonoBehaviour
{

    public int health;
    public int maxHealth;

    public TextMeshProUGUI healthText;

    void Start()
    {
        maxHealth = 10;
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

        healthText.text = health + " / " + maxHealth;

        health = Mathf.Clamp(health, 0, maxHealth);
    }

    public void DamageButton(int damageButton)
    {
        health -= damageButton;
    }

    public void HealButton(int healButton)
    {
        health += healButton;
    }
}
