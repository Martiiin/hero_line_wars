using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HeroLineWarsUnity
{
    public class PauseMenu : MonoBehaviour
    {
        public static bool GameIsPaused = false;

        public GameObject PauseMenuUI;

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                if(GameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }

        public void Resume()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().PlayMusic();
            PauseMenuUI.SetActive(false);
            GameIsPaused = false;
            Time.timeScale = 1;
        }

        void Pause()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().PauseMusic();
            PauseMenuUI.SetActive(true);
            GameIsPaused = true;
            Time.timeScale = 0;
        }

        public void Quit()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().StopMusic();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }
}
