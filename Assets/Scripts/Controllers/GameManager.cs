using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Locations;
using HeroLineWarsUnity.GestionVague;
using HeroLineWarsUnity.Dictionnaires;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using TMPro;

namespace HeroLineWarsUnity.Controller
{
    public class GameManager : MonoBehaviour
    {
        #region Variables
        public int vagueCourante;
        public int nbViesJoueur;
        public int tempsEntreVagues;
        public GameObject ScoreNomInput;

        public Camera mGameCamera;
        public Monster monstreFocus;

        public TextMeshProUGUI orText;
        public TextMeshProUGUI xpText;
        public TextMeshProUGUI vieText;
        public TextMeshProUGUI manaText;
        public TextMeshProUGUI attaqueText;
        public TextMeshProUGUI defenseText;
        public TextMeshProUGUI niveauText;

        public TextMeshProUGUI[] tableauObjectText;
        public TextMeshProUGUI objet1Text;
        public TextMeshProUGUI objet2Text;
        public TextMeshProUGUI objet3Text;
        public TextMeshProUGUI objet4Text;
        public TextMeshProUGUI objet5Text;
        public TextMeshProUGUI objet6Text;

        public TextMeshProUGUI[] tableauObjectTextVente;
        public TextMeshProUGUI objet1TextVente;
        public TextMeshProUGUI objet2TextVente;
        public TextMeshProUGUI objet3TextVente;
        public TextMeshProUGUI objet4TextVente;
        public TextMeshProUGUI objet5TextVente;
        public TextMeshProUGUI objet6TextVente;
        public bool scoreEnregistre;
        public bool finJeu;



        public Nexus mNexus;
        public GameObject mMonsterPrefab;
        public Transform[] mMonsterSpawners;

        public VaguesManager vaguesManager;
        public ObjectManager objectManager;
        private int _mNextMonsterSpawnersId = 0;
        public Collection<GameObject> _mMonsterInstances;

        // DEBUG : Timestamp o� le cooldown de spawn sera termin�
        private float _mNextSpawnTimestamp;

        #endregion

        

        void Start()
        {
            finJeu = false;
            Health health = GameObject.Find("HealthManager").GetComponent<Health>();
            GameObject.Find("MusicManager").GetComponent<MusicManager>().source.volume=0.1F;
            GameObject.Find("MusicManager").GetComponent<MusicManager>().PlayMusic();

            nbViesJoueur = health.maxHealth;
            tempsEntreVagues = 10;
            vagueCourante = 1;
            _mMonsterInstances = new Collection<GameObject>();
            _mNextSpawnTimestamp = Time.time + 2f; // Time.time = timestamp actuel
            vaguesManager = new VaguesManager();
            objectManager = new ObjectManager();

            tableauObjectText = new TextMeshProUGUI[6];

            tableauObjectText[0] = objet1Text;
            tableauObjectText[1] = objet2Text;
            tableauObjectText[2] = objet3Text;
            tableauObjectText[3] = objet4Text;
            tableauObjectText[4] = objet5Text;
            tableauObjectText[5] = objet6Text;

            tableauObjectTextVente = new TextMeshProUGUI[6];
            tableauObjectTextVente[0] = objet1TextVente;
            tableauObjectTextVente[1] = objet2TextVente;
            tableauObjectTextVente[2] = objet3TextVente;
            tableauObjectTextVente[3] = objet4TextVente;
            tableauObjectTextVente[4] = objet5TextVente;
            tableauObjectTextVente[5] = objet6TextVente;

            scoreEnregistre = false;

            GameObject.Find("PauseMenu").GetComponent<PauseMenu>().Resume();
        }

        // Update is called once per frame
        void Update()
        {

            orText.text = GameObject.Find("Hero").GetComponent<Hero>().or.ToString();
            xpText.text = GameObject.Find("Hero").GetComponent<Hero>().experience.ToString();
            defenseText.text = GameObject.Find("Hero").GetComponent<Hero>().defense.ToString();
            vieText.text = GameObject.Find("Hero").GetComponent<Hero>().vieActuelle.ToString();
            manaText.text = GameObject.Find("Hero").GetComponent<Hero>().manaActuel.ToString();
            attaqueText.text = GameObject.Find("Hero").GetComponent<Creature>().mDamage.ToString();
            niveauText.text = GameObject.Find("Hero").GetComponent<Creature>().mLevel.ToString();


            tableauObjectText[0] = objet1Text;
            tableauObjectText[1] = objet2Text;
            tableauObjectText[2] = objet3Text;
            tableauObjectText[3] = objet4Text;
            tableauObjectText[4] = objet5Text;
            tableauObjectText[5] = objet6Text;


            MiseAJourInventaireUI();


            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                Ray ray = mGameCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;
                if (Physics.Raycast(ray, out hitInfo))
                {
                    Monster clickedMonster = hitInfo.transform.gameObject.GetComponent<Monster>();

                    if (clickedMonster)
                    {
                        if (!monstreFocus)
                        {
                            monstreFocus = clickedMonster;
                        }

                        monstreFocus.GetComponent<Outline>().enabled = false;
                        clickedMonster.GetComponent<Outline>().enabled = true;
                        monstreFocus = clickedMonster;
                    }
                    else if(monstreFocus)
                    {
                        monstreFocus.GetComponent<Outline>().enabled = false;
                        monstreFocus = null;
                    }
                }
            }







            //On remet � jour la vie du nexus
            Health health = GameObject.Find("HealthManager").GetComponent<Health>();
            nbViesJoueur = health.health;
            // Debug.Log("TIME " + Mathf.Round(Time.time));
            if (nbViesJoueur == 0 || finJeu)
            {
                if (!scoreEnregistre)
                {
                    Time.timeScale = 0;
                    ScoreNomInput.SetActive(true);
                }
                
              
                
            }
            if (Mathf.Round(Time.time)% tempsEntreVagues == 0  && _mMonsterInstances.Count==0)
            {
                TourSuivant(vagueCourante);
            }

           

            // DEBUG : Spawn 1 montre toutes les 2 secs
            //if (Time.time >= _mNextSpawnTimestamp)
            //{
                //TESTING PURPOSES
                //CreateMonster("");
            //    _mNextSpawnTimestamp = Time.time + 2f;
            //}

        }


        void TourSuivant(int vagueCourante)
        {
            

            if (vaguesManager.MaxVague() == vagueCourante-1)
            {
                
                Debug.Log("FIN DU JEU");
                //Time.timeScale = 0;
                //ScoreNomInput.SetActive(true);
                finJeu = true;
            }
            else
            { 
                Vague vagueActuelle=vaguesManager.NouvelleVague(vagueCourante);
                foreach (GroupeMonstres groupe in vagueActuelle.MonstresDansVague)
                {
                    for (int i = 0; i < groupe.nombreMonstres; i++)
                    {
                        SpawnMonster(groupe.typeMonstre);
                    
                    }
                }
                this.vagueCourante += 1;
             return;
            }
        }

        bool GetJoueurVivant()
        {
            return true;
        }

        public void CreateMonster(string monsterType)
        {
            // S�lection du spawner � utiliser, et incr�mentation de l'indice du prochain
            Transform nextSpawner = mMonsterSpawners[_mNextMonsterSpawnersId];
            _mNextMonsterSpawnersId = (_mNextMonsterSpawnersId + 1) % mMonsterSpawners.Length;


            // Cr�ation du GameObject repr�sentant le monstre
            mMonsterPrefab = Resources.Load<GameObject>("GruntHP");
            GameObject monsterInstance = Instantiate(mMonsterPrefab, nextSpawner.position, new Quaternion());
            _mMonsterInstances.Add(monsterInstance);

            // Config du monstre
            monsterInstance.GetComponent<Monster>().mDefaultTarget = mNexus;
        }

        public void SpawnMonster(MonsterParameters monsterParameters)
        {
            // S�lection du spawner � utiliser, et incr�mentation de l'indice du prochain
            Transform nextSpawner = mMonsterSpawners[_mNextMonsterSpawnersId];
            _mNextMonsterSpawnersId = (_mNextMonsterSpawnersId + 1) % mMonsterSpawners.Length;


            // Cr�ation du GameObject repr�sentant le monstre
            mMonsterPrefab = monsterParameters.monsterPrefab;
            
            GameObject monsterInstance = Instantiate(mMonsterPrefab, nextSpawner.position, new Quaternion());
            _mMonsterInstances.Add(monsterInstance);

            // Config du monstre
            monsterInstance.GetComponent<Monster>().nom = monsterParameters.nom;
            monsterInstance.GetComponent<Monster>().vie = monsterParameters.vie;
            monsterInstance.GetComponent<Monster>().vieActuelle = monsterParameters.vie;
            monsterInstance.GetComponent<Monster>().defense = monsterParameters.defense;
            monsterInstance.GetComponent<Monster>().or = monsterParameters.or;
            monsterInstance.GetComponent<Monster>().experience = monsterParameters.experience;
            monsterInstance.GetComponent<Monster>().vitesse = monsterParameters.vitesse;
            monsterInstance.GetComponent<Monster>().mDefaultTarget = mNexus;
            
        }

        public int GetNbViesJoueur()
        {
            return nbViesJoueur;
        }

        public void removeHeroTarget(IAttackable cible)
        {
            if (cible == GameObject.Find("Hero").GetComponent<Hero>().mTarget)
            {
                GameObject.Find("Hero").GetComponent<Hero>().mTarget = null;
            }
        }

        public void MiseAJourInventaireUI()
        {
            for (int i = 0; i < GameObject.Find("Hero").GetComponent<Hero>().inventaire.Length; i++) 
            {
                if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[i] != null)
                {
                    tableauObjectText[i].text = GameObject.Find("Hero").GetComponent<Hero>().inventaire[i].nom;
                    tableauObjectTextVente[i].text = GameObject.Find("Hero").GetComponent<Hero>().inventaire[i].nom;
                }
                else
                {
                    tableauObjectText[i].text = "";
                    tableauObjectTextVente[i].text = "";
                }
            }
        }
    }
}




