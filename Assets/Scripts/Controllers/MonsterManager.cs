using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Locations;

namespace HeroLineWarsUnity.Controller
{
    public class MonsterManager : MonoBehaviour
    {
        #region Variables

        public  Nexus mNexus;
        public  GameObject mMonsterPrefab;
        public  Transform[] mMonsterSpawners;

        private  int _mNextMonsterSpawnersId = 0;
        private  Collection<GameObject> _mMonsterInstances;

        // DEBUG : Timestamp o� le cooldown de spawn sera termin�
        private float _mNextSpawnTimestamp;

        #endregion

        #region Built-in methods
        // Start is called before the first frame update
        void Start()
        {
            _mMonsterInstances = new Collection<GameObject>();
            _mNextSpawnTimestamp = Time.time + 2f; // Time.time = timestamp actuel
        }

        // Update is called once per frame
        void Update()
        {
            // DEBUG : Spawn 1 montre toutes les 2 secs
            if (Time.time >= _mNextSpawnTimestamp)
             {
             CreateMonster("");
             _mNextSpawnTimestamp = Time.time + 2f;
            }
        }

        #endregion

        #region Custom methods

        public  void CreateMonster(string monsterType)
        {
            // S�lection du spawner � utiliser, et incr�mentation de l'indice du prochain
            Transform nextSpawner = mMonsterSpawners[_mNextMonsterSpawnersId];
            _mNextMonsterSpawnersId = (_mNextMonsterSpawnersId + 1) % mMonsterSpawners.Length;


            // Cr�ation du GameObject repr�sentant le monstre
            GameObject monsterInstance = Instantiate(mMonsterPrefab, nextSpawner.position, new Quaternion());
            _mMonsterInstances.Add(monsterInstance);

            // Config du monstre
            monsterInstance.GetComponent<Monster>().mDefaultTarget = mNexus;
        }


        public  void SpawnMonster(Monster monster)
        {
            // S�lection du spawner � utiliser, et incr�mentation de l'indice du prochain
            Transform nextSpawner = mMonsterSpawners[_mNextMonsterSpawnersId];
            _mNextMonsterSpawnersId = (_mNextMonsterSpawnersId + 1) % mMonsterSpawners.Length;


            // Cr�ation du GameObject repr�sentant le monstre
            GameObject monsterInstance = Instantiate(monster.monsterPrefab, nextSpawner.position, new Quaternion());
            _mMonsterInstances.Add(monsterInstance);

            // Config du monstre
            monsterInstance.GetComponent<Monster>().mDefaultTarget = mNexus;
        }

        #endregion
    }
}
