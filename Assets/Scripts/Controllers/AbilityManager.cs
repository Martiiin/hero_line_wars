using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using HeroLineWarsUnity.Abilite;

namespace HeroLineWarsUnity.Controller
{
    public class AbilityManager : MonoBehaviour
    {
        private Ability _mCurrentAbility;
        

        public void Update()
        {
            
            if (IsAbilityStillExecuting())
            {
                ExecuteStrategy();
            }
        }

        public void SetStrategy(Ability newAbility)
        {
            
            _mCurrentAbility = newAbility;
            _mCurrentAbility.SetEndTimestamp();
            
            
        }

        public bool IsAbilityStillExecuting()
        {
            if (_mCurrentAbility == null) return false;
            float currentTime = Time.time;
            return currentTime <= _mCurrentAbility.mEndTimestamp;
            
        }

        public void ExecuteStrategy()
        {
            _mCurrentAbility.Execute();
        }
    }
}