using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class MusicManager : MonoBehaviour
    {

        public AudioSource source;

 
        public void PlayMusic()
        {
            source.volume = 0.2f;
            source.Play();
        }

        public void StopMusic()
        {
            source.Stop();
        }

        public void PauseMusic()
        {
            source.Pause();
        }
    }
}
