using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Dictionnaires;
using HeroLineWarsUnity.GestionVague;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Factories;
using System.Collections.ObjectModel;
using HeroLineWarsUnity.Locations;


namespace HeroLineWarsUnity.Controller
{
    public class VaguesManager
    {
        public Nexus mNexus;
        public GameObject mMonsterPrefab;
        public Transform[] mMonsterSpawners;

        private int _mNextMonsterSpawnersId = 0;
        private Collection<GameObject> _mMonsterInstances;



        //public bool vagueTerminee;
        public int derniereVague;
        public  Vague vagueActuelle;
        public DictionnaireVagues dictionnaireVagues;
        public DictionnaireMonstres dictionnaireMonstres;
        // Start is called before the first frame update

       public VaguesManager()
        {
            //vagueTerminee = true;
            derniereVague = 0;
            
            dictionnaireMonstres = new DictionnaireMonstres();
            dictionnaireVagues = new DictionnaireVagues(dictionnaireMonstres);

            
        }

            //  MonsterFactory monsterfactory = new MonsterFactory() ;
            //  Debug.Log("NOM ???? " + monsterfactory.getMonster(dictionnaireMonstres,"Goblin").nom);
            //  Vague vagueActuelle = DictionnaireVagues.ListeVagues[0];
            //  Debug.Log("num�ro " + vagueActuelle.numVague);
            // Debug.Log("Groupe " + vagueActuelle.MonstresDansVague.ToString());
        

        public bool VagueEnCours(int vaguenumero)
        {
            return vaguenumero == this.derniereVague;
        }

        public Vague NouvelleVague(int vagueNumero)
        {
           

            Debug.Log("vague num�ro" + +vagueNumero);
            
             Vague vagueActuelle = dictionnaireVagues.getVagueByNum(vagueNumero);
             Debug.Log("Vague cr��e");
            if (vagueActuelle == null)
            {
                Debug.Log("null :/");
                return null;
            }
            else
            {
                this.derniereVague += 1;
                return vagueActuelle;
            }
        }

        public int MaxVague()
        {
            return dictionnaireVagues.ListeVagues.Count;
        }

    }

}
