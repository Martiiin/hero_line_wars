using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HeroLineWarsUnity
{
    public class MainMenu : MonoBehaviour
    {
        public void PlayGame()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().StopMusic();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

        public void QuitGame()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().PlayMusic();
            Application.Quit();
        }
    }
}
