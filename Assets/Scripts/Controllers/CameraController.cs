using UnityEngine;
namespace HeroLineWarsUnity.Controller
{
    public class CameraController : MonoBehaviour
    {
        public float panSpeed = 20f;
        public float panBorder = 10f;
        public float scrollSpeed = 2f;
        public float maxY = 120f;
        public float minY = 12f;

        public Vector2 panLimit;
        // Update is called once per frame
        void Update()
        {
            Vector3 pos = transform.position;

            if (Input.GetKey("z") || Input.mousePosition.y >= Screen.height - panBorder)
            {
                pos.z += panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("s") || Input.mousePosition.y <= panBorder)
            {
                pos.z -= panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorder)
            {
                pos.x += panSpeed * Time.deltaTime;
            }
            if (Input.GetKey("q") || Input.mousePosition.x <= panBorder)
            {
                pos.x -= panSpeed * Time.deltaTime;
            }

            float scroll = Input.GetAxis("Mouse ScrollWheel");
            pos.y -= scroll * scrollSpeed * Time.deltaTime * 100f;

            pos.x = Mathf.Clamp(pos.x, -panLimit.x+800, panLimit.x+800);
            pos.y = Mathf.Clamp(pos.y, minY, maxY);
            pos.z = Mathf.Clamp(pos.z, -panLimit.y+740, panLimit.y+740);

            transform.position = pos;
        }
    }
}