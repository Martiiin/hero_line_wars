using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Objet;

namespace HeroLineWarsUnity.Factories
{
    public interface ObjetInventaireFactory 
    {
        public ObjetInventaire CreeObjetInvenaitre();
    }
}
