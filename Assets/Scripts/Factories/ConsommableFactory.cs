using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Factories;
using HeroLineWarsUnity.Objet;

namespace HeroLineWarsUnity
{
    public interface ConsommableFactory : ObjetInventaireFactory
    {
        public new ObjetInventaire CreeObjetInvenaitre() {
            return CreeConsommable();
        }

        public Consommable CreeConsommable();
    }
}
