using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Factories;
using HeroLineWarsUnity.Objet;

namespace HeroLineWarsUnity
{
    public class EquipementFactory : ObjetInventaireFactory
    {
        int bonusVie;
        int bonusMana;
        int bonusDefense;
        int bonusDommage;
        string nom;
        int prix;
        int revente;

        public ObjetInventaire CreeObjetInvenaitre()
        {
            return new Equipement(nom, prix, revente, bonusVie, bonusMana, bonusDefense, bonusDommage);
        }

        public EquipementFactory(string nom,int prix,int revente, int bonusVie,int bonusMana,int bonusDefense,int bonusDommage)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.bonusVie = bonusVie;
            this.bonusMana = bonusMana;
            this.bonusDefense = bonusDefense;
            this.bonusDommage = bonusDommage;
        }
    }
}
