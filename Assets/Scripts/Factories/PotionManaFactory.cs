using HeroLineWarsUnity.Objet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class PotionManaFactory : ConsommableFactory
    {
        // Start is called before the first frame update
        int manaRendu;
        string nom;
        int prix;
        int revente;

        public PotionManaFactory(string nom, int prix, int revente, int manaRendu)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.manaRendu = manaRendu;
        }
        public Consommable CreeConsommable()
        {
            return new PotionMana(nom, prix, revente, manaRendu);
        }

        public ObjetInventaire CreeObjetInvenaitre()
        {
            return CreeConsommable();
        }
    }
}
