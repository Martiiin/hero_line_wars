using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Dictionnaires;

namespace HeroLineWarsUnity.Factories
{
    public class MonsterFactory 
    {

        public  MonsterParameters getMonster(DictionnaireMonstres dico, string monsterName)
        {
           return dico.getMonsterModelByName(monsterName);
        }
    }
}
