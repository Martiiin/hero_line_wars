using HeroLineWarsUnity.Objet;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class PotionVieFactory : ConsommableFactory 
    {
        // Start is called before the first frame update
        int vieRendue;
        string nom;
        int prix;
        int revente;

        public PotionVieFactory (string nom,int prix,int revente,int vieRendue)
        {
            this.nom = nom;
            this.prix = prix;
            this.revente = revente;
            this.vieRendue = vieRendue;
        }
        public Consommable CreeConsommable()
        {
            return new PotionVie(nom, prix, revente, vieRendue);
        }

        public ObjetInventaire CreeObjetInvenaitre()
        {
            return CreeConsommable();
        }
    }
}
