using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Controller;
using HeroLineWarsUnity.Objet;

namespace HeroLineWarsUnity
{
    public class VendreObjet : MonoBehaviour
    {
        public void VendreObjet1()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[0] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[0].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(0);
            }
        }

        public void VendreObjet2()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[1] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[1].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(1);
            }
        }

        public void VendreObjet3()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[2] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[2].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(2);
            }
        }


        public void VendreObjet4()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[3] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[3].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(3);
            }
        }


        public void VendreObjet5()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[4] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[4].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(4);
            }
        }

        public void VendreObjet6()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[5] != null)
            {
                GameObject.Find("Hero").GetComponent<Hero>().or += GameObject.Find("Hero").GetComponent<Hero>().inventaire[5].revente;
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.RetirerObjetDeLinventaire(5);
            }
        }

    }
}
