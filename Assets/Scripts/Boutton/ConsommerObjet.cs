using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Creatures;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Interfaces;

namespace HeroLineWarsUnity
{
    public class ConsommerObjet : MonoBehaviour
    {
        public void ConsommerObjet1()
        {
            if(GameObject.Find("Hero").GetComponent<Hero>().inventaire[0] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[0] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[0]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[0]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[0] = null;
                }
            }
        }
        public void ConsommerObjet2()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[1] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[1] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[1]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[1]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[1] = null;
                }
            }
        }
        public void ConsommerObjet3()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[2] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[2] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[2]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[2]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[2] = null;
                }
            }
        }
        public void ConsommerObjet4()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[3] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[3] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[3]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[3]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[3] = null;
                }
            }
        }
        public void ConsommerObjet5()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[4] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[4] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[4]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[4]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[4] = null;
                }
            }
        }

        public void ConsommerObjet6()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().inventaire[5] != null && GameObject.Find("Hero").GetComponent<Hero>().inventaire[5] is IConsommable)
            {
                ((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[5]).consommerCharge();
                if (((Consommable)GameObject.Find("Hero").GetComponent<Hero>().inventaire[5]).nombreCharge <= 0)
                {
                    GameObject.Find("Hero").GetComponent<Hero>().inventaire[5] = null;
                }
            }
        }




    }
}
