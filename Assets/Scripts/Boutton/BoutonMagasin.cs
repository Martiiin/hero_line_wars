using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity.Boutton
{

  
    public class BoutonMagasin : MonoBehaviour
    {
        public GameObject canvasMagasin;

        public void OuvrirFenetreMagasin()
        {
            if(canvasMagasin != null) {
             bool isActive = canvasMagasin.activeSelf;
            canvasMagasin.SetActive(!isActive);
            }   
        }
    }
}
