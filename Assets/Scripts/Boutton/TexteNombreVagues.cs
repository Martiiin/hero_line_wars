using HeroLineWarsUnity.Controller;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace HeroLineWarsUnity
{

    
    public class TexteNombreVagues : MonoBehaviour
    {
       public GameObject gameManager;
       public int nombreVague;
       public TextMeshProUGUI textVague;
        public TextMeshProUGUI textVagueMax;
        public TextMeshProUGUI textPseudoMax;


        private void Update()
        {
            EnregistrerNombreVagues();
        }
        public void EnregistrerNombreVagues()
        {

            nombreVague=gameManager.GetComponent<GameManager>().vagueCourante-1;
            textVague.text = nombreVague.ToString();
            PlayerPrefs.SetInt("Vague", nombreVague);
            PlayerPrefs.Save();
            if(PlayerPrefs.GetString("Meilleur Pseudo") != null)
            {
                textPseudoMax.text = PlayerPrefs.GetString("Meilleur Pseudo");
            }
            if (PlayerPrefs.GetInt("Meilleur Vague") != 0)
            {
                textVagueMax.text = PlayerPrefs.GetInt("Meilleur Vague").ToString();
            }
            else
            {
                PlayerPrefs.SetInt("Meilleur Vague", 1);
                PlayerPrefs.Save();
                textVagueMax.text = "1";
            }
        }
    }
}
