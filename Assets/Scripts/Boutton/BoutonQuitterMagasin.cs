using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class BoutonQuitterMagasin : MonoBehaviour
    {
        public GameObject canvasMagasin;
        public GameObject canvasVente;
        public void FermerFenetreMagasin()
        {
            if (canvasMagasin != null)
            {
                bool isActive = canvasMagasin.activeSelf;
                canvasMagasin.SetActive(!isActive);
            }
        }


        public void FermerFenetreVente()
        {
            if (canvasVente != null)
            {
                bool isActive = canvasVente.activeSelf;
                canvasVente.SetActive(!isActive);
            }
        }



        public void  ChangerFenetreVenteEtMagasin()
        {
            if (canvasMagasin != null && canvasVente != null)
            {
                bool isActiveMagasin = canvasMagasin.activeSelf;
                canvasMagasin.SetActive(!isActiveMagasin);
                bool isActiveVente = canvasVente.activeSelf;
                canvasVente.SetActive(!isActiveVente);
            }
        }
    }
}
