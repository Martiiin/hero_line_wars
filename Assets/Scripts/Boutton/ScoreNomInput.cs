using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HeroLineWarsUnity
{
    public class ScoreNomInput : MonoBehaviour
    {

        //public string input="";
        //public GameObject inputObject;
        private string input;

        private void Start()
        {
            
        }
        private void Update()
        {
            
        }
        public void LireInputNom(string input)
        {
            //input = inputObject.GetComponent<UnityEngine.UI.Text>().text;
            Debug.LogWarning("Pseudo " + input);
            PlayerPrefs.SetString("Pseudo", input);
            PlayerPrefs.Save();

            if (PlayerPrefs.GetInt("Meilleur Vague") == 0)
            {
                Debug.LogWarning("DANS LE 0");
                PlayerPrefs.SetString("Meilleur Pseudo", PlayerPrefs.GetString("Pseudo"));
                PlayerPrefs.SetInt("Meilleur Vague", PlayerPrefs.GetInt("1"));
                PlayerPrefs.Save();
            }

            if (PlayerPrefs.GetInt("Meilleur Vague") != 0 && PlayerPrefs.GetInt("Meilleur Vague") < PlayerPrefs.GetInt("Vague"))
            {
                PlayerPrefs.SetString("Meilleur Pseudo", PlayerPrefs.GetString("Pseudo"));
                PlayerPrefs.SetInt("Meilleur Vague", PlayerPrefs.GetInt("Vague"));
                PlayerPrefs.Save();
            }

        }

    }


}
