using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class BoutonVendre : MonoBehaviour
    {
        public GameObject canvasVendre;

        public void OuvrirFenetreMagasin()
        {
            if (canvasVendre != null)
            {
                bool isActive = canvasVendre.activeSelf;
                canvasVendre.SetActive(!isActive);
            }
        }
    }
}
