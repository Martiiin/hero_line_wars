using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeroLineWarsUnity.Objet;
using HeroLineWarsUnity.Controller;
using HeroLineWarsUnity.Creatures;

namespace HeroLineWarsUnity
{
    public class AcheterObjet : MonoBehaviour
    {

        public void AcheterPotionVie()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion vie").prix)
            {
                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }


                ObjetInventaire potionDeVie = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion vie").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(potionDeVie);
                Debug.Log("Objet Achete");
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion vie").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcherterPotionMana()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion mana").prix)
            {

                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }

                ObjetInventaire potionDeMana = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion mana").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(potionDeMana);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("grande potion mana").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcheterHache()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("hache").prix)
            {
                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }

                ObjetInventaire hache = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("hache").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(hache);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("hache").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcheterCasque()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("casque").prix)
            {

                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }

                ObjetInventaire casque = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("casque").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(casque);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("casque").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcheterArmure()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("armure").prix)
            {
                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }


                ObjetInventaire armure = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("armure").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(armure);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("armure").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcheterGants()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("gants").prix)
            {

                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }

                ObjetInventaire gants = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("gants").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(gants);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("gants").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }

        public void AcheterBottes()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bottes").prix)
            {
                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }


                ObjetInventaire bottes = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bottes").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(bottes);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bottes").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }
        public void AcheterBouclier()
        {
            if (GameObject.Find("Hero").GetComponent<Hero>().or >= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bouclier").prix)
            {
                if (GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.InventairePlein())
                {
                    return;
                }


                ObjetInventaire bouclier = GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bouclier").Clone();
                GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.AjouterObjetDansInventaire(bouclier);
                GameObject.Find("Hero").GetComponent<Hero>().or -= GameObject.Find("GameManager").GetComponent<GameManager>().objectManager.dictionnaireObjets.GetObjetByNom("bouclier").prix;
            }
            else
            {
                //todo : popup or message rouge PAS ASSEZ D'OR
            }
        }
    }
}
