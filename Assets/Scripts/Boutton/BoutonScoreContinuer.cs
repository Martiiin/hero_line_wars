using HeroLineWarsUnity.Controller;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HeroLineWarsUnity
{
    public class BoutonScoreContinuer : MonoBehaviour
    {
        public GameObject GameOver;
        public GameObject ScoreInput;
        public GameObject GameManager;
        public void ContinuerScore()
        {
            ScoreInput.SetActive(false);
            GameManager.GetComponent<GameManager>().scoreEnregistre = true;
            
            GameOver.SetActive(true);
        }
    }
}
