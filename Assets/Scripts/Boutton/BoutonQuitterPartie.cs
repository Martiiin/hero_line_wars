using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace HeroLineWarsUnity
{
    public class BoutonQuitterPartie : MonoBehaviour
    {
        public void quiterPartie()
        {
            GameObject.Find("MusicManager").GetComponent<MusicManager>().StopMusic();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
    }
}
